<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterReportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {

        Schema::table('reports', function(Blueprint $table)
        {

            if (Schema::hasColumn('restaurant', 'kommun','peopleSickCount'))
            {


                $table->dropColumn(
                    'restaurant','kommun','peopleSickCount'
                );

            }
        });

        Schema::table('reports', function(Blueprint $table)
        {
            $table->integer('city_id')->index();
            $table->integer('restaurant_id')->index();
            $table->timestamp('ate_at')->nullable();  
            $table->integer('people_sick_count');
            $table->time('time_until_symptoms');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('reports', function(Blueprint $table)
        {
            $table->dropColumn(
               'city_id',
               'restaurant_id',
               'ate_at',
               'people_sick_count',
               'time_until_symptoms'
            );

        });
    }
}
