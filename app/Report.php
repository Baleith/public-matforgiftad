<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Report extends Model
{
    protected $fillable = ['city_id','restaurant_id','date','ate_at','medical_attention','time_until_symptoms','people_sick_count'];
}
