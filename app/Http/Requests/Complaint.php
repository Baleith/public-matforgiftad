<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;

class Complaint extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'date' => 'required|date',
            'restaurant' => 'required',
            'sickCount' => 'required|numeric',
            'kommun' => 'required',
            'name' => 'nullable|string',
            'phone' => 'numeric|nullable',
            'email' => 'email|nullable',
            'emailConfirmed' => 'same:email|nullable',
            'sickFood' => 'required',
            'care' => 'required',
            'allergy' => 'required',
            'streetaddress' => 'required',
            'time' => 'required',
            'symptoms' => 'required',
            'anonymous' => 'required',
            'timeUntilSymptoms' => 'required',
        ];

    }

    public function withValidator(Validator $validator)
    {
        $validator->sometimes('gpdr', 'accepted', function ($input) {
            return $input->anonymous !== true;
        });
        //return $input->name !== null || $input->phone !== null || $input->email !== null;
    }

    public function messages()
    {
        return [
            'gpdr.accepted' => 'Du måste acceptera att vi får skicka dina personuppgifter',
            'phone.numeric'  => 'Ange numret med siffor',
            'email.email' => 'E-postadressen är inte en giltlig e-post',
            'emailConfirmed.same'  => 'E-postadreserna måste stämma överens',
            'sickCount.required'  => 'Antal insjuknade måste anges',
            'sickCount.numeric'  => 'Ange antal insjuknade med siffor',
            'sickFood.required'  => 'Mat måste anges',
            'symptoms.required'  => 'Symptom måste anges',
            'care.required'  => 'Vård måste anges',
            'allergy.required'  => 'Allergi måste anges',
            'restaurant.required'  => 'Restaurang måste anges',
            'streetaddress.required'  => 'Gatuadress måste anges',
            'date.required'  => 'Datum måste anges',
            'time.required'  => 'Tid måste anges',
            'kommun.required'  => 'Kommun måste anges',
            'anonymous.required'  => 'Ange om du vill vara anonym eller ej',
            'timeUntilSymptoms.required' => 'Ange hur lång tid det tog för att uppvisa symptom'
        ];
    }
}
