<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\City;

class ApiController extends Controller
{
	public function getCities()
	{
		return City::select(\DB::raw('name AS label,id AS value'))->get();
	}
}
