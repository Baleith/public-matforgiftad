<?php

namespace App\Http\Controllers;

use App\City;
use App\Http\Requests\Complaint;
use App\Mail\ComplaintOriginal;
use App\Mail\ComplaintCopy;
use App\Report;
use Illuminate\Support\Facades\Mail;
use \Illuminate\Support\Facades\URL;
use App\Restaurant;
use App\Symptom;
use App\ReportSymptom;
use DB;


class ReportController extends Controller
{
    public function getOrStoreRestaurant($restaurantName,$cityId)
    {
        $restaurant = Restaurant::where('name','=',$restaurantName)->where('city_id','=',$cityId)->first();

        if($restaurant)
        {
            return $restaurant;
        }

        $restaurant = new Restaurant();
        $restaurant->name = $restaurantName;
        $restaurant->city_id = $cityId;
        $restaurant->save();
        return $restaurant;

    }
    public function storeSymptoms($symptoms,$report)
    {
        foreach($symptoms as $symptom)
        {
            $dbSymptom = Symptom::where('illness',$symptom)->first();

            if($dbSymptom)
            {
                $reportSymptom = new ReportSymptom();
                $reportSymptom->symptom_id = $dbSymptom->id;
                $reportSymptom->report_id = $report->id;
                $reportSymptom->save();
            }

        }
    }
    public function store(Complaint $complaint)
    {
        $kommun = City::findOrFail($complaint->kommun['value']);

        DB::transaction(function() use ($complaint, $kommun) {
            $restaurant = $this->getOrStoreRestaurant($complaint->restaurant,$kommun->id);

            $report = Report::create([
                'city_id' => $kommun->id,
                'restaurant_id' => $restaurant->id,
                'date' => $complaint->date,
                'ate_at' => $complaint->date,
                'medical_attention' => $complaint->care,
                'time_until_symptoms' => $complaint->timeUntilSymptoms,
                'people_sick_count' => $complaint->sickCount,
            ]);

            $this->storeSymptoms($complaint->symptoms,$report);
        });

        if($complaint->filled(['checkCopy', 'email', 'gdpr'])) {
            Mail::send(new ComplaintCopy($complaint));

            $response['email'] = $complaint->email;
        }
        Mail::send(new ComplaintOriginal($complaint, $kommun->email));

        $url = URL::temporarySignedRoute(
            'skickad', now()->addMinutes(15)
        );
        return response()->json($url);
    }
}
