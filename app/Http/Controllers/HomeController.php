<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Bugsnag\BugsnagLaravel\Facades\Bugsnag;

class HomeController extends Controller
{
    public function index()
    {
        $title = 'Hem';
        $desc = 'Välkommen';
        $meta = array('title' => 'Matförgiftad.se | Anmäl misstänkt matförgiftning','description' => 'Anmäl matförgiftning. Det är många som drabbas årligen. Anmäl det till ansvarig kommun.');

        return view('home', compact('title', 'desc','meta'));
    }

    public function getFacts()
    {
        $title = 'Fakta';
        $desc = 'Mer om matförgiftning';
        $meta = array('title' => 'Fakta om matförgiftning | Matförgiftad.se','description' => 'Matförgiftning kan man få av många olika bakterier och virus. Symtomen kan variera något, men oftast kräks man och har diarré. Det är viktigt att ersätta den.');

        return view('facts', compact('title', 'desc','meta'));
    }

    public function contactUs()
    {
        $title = 'Kontakta';
        $desc = 'Hör gärna av dig';
        $meta = array('title' => 'Kontakta oss | Matförgiftad.se','description' => 'Kontakta oss på matförgiftad.se och ge oss feedback på vår tjänst.');
        return view('contact-us', compact('title', 'desc','meta'));
    }

    public function report()
    {
        $title = 'Anmäl';
        $desc = 'Gör din anmälan';
        $meta = array('title' => 'Anmäl om du tror att du blivit matförgiftad | Matförgiftad.se','description' => 'Är du en av de 500 000 svenskar som blir matförgiftade varje år? Gör din anmälan här så skickas den snabbt och enkelt till den ansvariga i kommunen.');
        return view('report', compact('title', 'desc','meta'));
    }

    public function aboutUs()
    {

        $title = 'Om oss';
        $desc = 'Vad är Matfögiftad.se';
        $meta = array('title' => 'Om matförgiftad.se | Matförgiftad.se','description' => 'Matförgiftad.se är en tjänst där du enkelt kan anmäla om du misstänker att du blivit matförgiftad på restaurang, café eller annan servering. Anmälan skickas till');
        return view('about-us', compact('title', 'desc','meta'));
    }

    public function sent(Request $request)
    {
        if (! $request->hasValidSignature()) {
            return redirect('/rapportera');
        }
        $title = 'Anmäl';
        $desc = 'Anmälan skickad';

        $meta = array('title' => 'Anmälan skickad | Matförgiftad.se','description' => 'Tack! Din anmälan är mottagen');

        return view('complaint-successful', compact('title', 'desc','meta'));
    }
}
