<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ComplaintOriginal extends Mailable
{
    use Queueable, SerializesModels;

    protected $data, $email;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($formData, $email)
    {
        $this->data = $formData;
        $this->email = $email;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('anmalan@matforgiftad.se')
        ->to($this->email)
        ->subject('Anmälan Matförgiftning')
        ->markdown('emails.complaintOriginal')
        ->with(['user_info' => $this->data]);
    }
}
