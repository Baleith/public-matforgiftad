@extends('layouts.app')

@section('content')

    <div class="container">



        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Har du blivit matförgiftad?</h1>

                <p>
                    Är du en av de 500 000 svenskar som blir matförgiftade varje år?
                    Gör din anmälan här så skickas den snabbt och enkelt till den ansvariga i kommunen där matstället finns.
                </p>
                <a class="btn btn-primary btn-lg" href="/rapportera" role="button">Anmäl här</a>

                <p>
                    Varför ska man anmäla om man blivit matförgiftad?
                    En snabb anmälan kan hindra att andra blir sjuka. Om matförgiftningar anmäls och utreds blir det möjligt att ta reda på orsaker till utbrott och skaffa sig kunskap om hur de kan förebyggas. Uppskattningsvis drabbas varje år 500 000 personer av matförgiftningar efter att ha ätit mat hemma eller på restaurang i Sverige, och ytterligare 250 000 efter utlandsvistelser.
                </p>


                <h2> Tjänsten Matförgiftad </h2>

                <p> Matförgiftad.se är en tjänst där du enkelt kan anmäla om du misstänker att du blivit matförgiftad.
                    Anmälan skickas till den ansvariga kommunen och du väljer själv om du vill vara anonym eller inte. Kom dock ihåg att anonymitet har sin nackdel, kommunen som utreder fallet kan behöva komma i kontakt med dig för ytterligare information, därför är det jättebra ifall du vill lämna kontaktuppgifter till dem. <br>
                    Matförgiftad.se lagrar inga personuppgifter så som namn, epost eller telefonnummer utan för oss kommer du alltid förbli anonym.

                </p>


            </div>
        </div>
    </div>

@endsection
