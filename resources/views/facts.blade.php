@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1>Fakta om matförgiftning</h1>
                <p>
                    Matförgiftning kan man få av många olika bakterier och virus. Symtomen kan variera något, men oftast kräks man och har diarré. Det är viktigt att ersätta den förlorade vätskan på rätt sätt. Vätskeersättning kan du köpa på apoteket. Förebygg matförgiftning genom att tvätta händerna ofta och att hantera mat väl.
                </p>

                <p>
                    Matförgiftning är trolig orsak om fler än två personer som ätit av samma mat får symtom som illamående, kräkningar, magsmärtor och diarré.
                </p>

                <p>
                    Klassisk matförgiftning orsakas av gifter, toxiner, som bildas när bakterier växer till i mat som inte förvarats på ett korrekt sätt. Matförgiftning kan också orsakas av virus. Bakterier som salmonella, campylobacter och ETEC-bakterien, som är den vanligaste orsaken till så kallad turistdiarré, kan också ge upphov till symtom som kan vara svåra att skilja från symtomen vid matförgiftning. Men i dessa fall brukar tiden fram till insjuknandet vara något längre. Det förekommer dock att man, i synnerhet vid utlandsresa till länder med lägre hygienisk standard, smittas av dessa bakterier samtidigt som man får matförgiftning.
                </p>

                <p>
                    Om du tror att du har blivit matförgiftad av orsaker som inte beror på att du själv hanterat maten olämpligt bör du kontakta din kommun. Kontakta gärna även det ställe där du har ätit så att de kan förhindra att fler smittas.
                </p>

                <h2>Symtom på matförgiftning</h2>

                <p>
                    Det tar mellan en och 48 timmar från det att man ätit den infekterade maten till dess att symtomen visar sig, beroende på vilket smittämne som ligger bakom. Insjuknandet blir ofta dramatiskt och man mår ofta mycket dåligt med illamående, kräkningar, diarréer och ont i magen.
                </p>

                <p>
                    Matförgiftningar som orsakas av bakterier går över av sig själva efter mellan 12 timmar och två dygn. Virusinfektioner kan ibland vara längre.
                </p>

                <p>
                    Alla typer av matvaror som inte hanterats väl kan orsaka matförgiftning. Vissa maträtter är oftare orsak till matförgiftning än andra. Gemensamt är dock att maten har hanterats på ett felaktigt sätt så att bakterierna vuxit till och/eller att personer som tillagat eller hanterat maten brustit i hygien, vanligen inte tvättat händerna.
                </p>

                <ul>
                    <li>Bakverk, såser, dressing, smörgåsar och konserverad skinka kan innehålla gift som bildats av en stafylokock, en mycket vanlig hudbakterie. Efter en till sex timmar mår den drabbade illa och kräks, får ont i magen och diarré.</li>
                    <li>Tillagat kött, fågel eller fisk som återuppvärmts kan vara smittat med gift från en klostridiebakterie. Symtomen kommer efter 8-24 timmar och ger oftast vattentunn diarré och magont.</li>
                    <li>Stekt ris kan innehålla gift från en bakterie som ger kräkningar, diarréer och magsmärtor 1-16 timmar efter en måltid.</li>
                    <li>I rå fisk och råa skaldjur kan det finnas en bakterie som är släkt med kolerabakterien. Om du utsätts för denna bakterie kan du få feber och diarréer blandade med blod och slem.</li>
                    <li>Vissa virus kan också smitta via många olika maträtter och ge matförgiftning. Många av dessa ger liknande symtom som bakterier med kräkningar, diarré och magont.</li>
                </ul>

                <h2>Statistik</h2>
                <p>
                    Varje år blir uppskattningsvis 500 000 svenskar matförgiftade. Av dessa är det bara mellan 2000 och 3000 fall som anmäls till Sveriges kommuner eller Smittskyddsinstitutet.
                </p>

                <h4>Källor</h4>

                <a href="http://www.vardguiden.se/Sjukdomar-och-rad/Omraden/Sjukdomar-och-besvar/Matforgiftning/">www.vardguiden.se/Sjukdomar-och-rad/Omraden/Sjukdomar-och-besvar/Matforgiftning/</a>
                <br >
                <br >
                <a href="http://www.slv.se/sv/grupp1/Risker-med-mat/Matforgiftningar/Hur-manga-drabbas-av-matforgiftning/">www.slv.se/sv/grupp1/Risker-med-mat/Matforgiftningar/Hur-manga-drabbas-av-matforgiftning/</a>
                <br >
                <br >
                Läs mer på <a href="https://sv.wikipedia.org/wiki/Matförgiftning">sv.wikipedia.org/wiki/Matförgiftning</a>
            </div>
        </div>
    </div>

@endsection
