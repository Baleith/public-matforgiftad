@extends('layouts.app')

@section('content')

    <div class="container">



        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Denna sida kunde tyvärr inte hittas</h1>

                <p>
                    Rackarns bananer... Något gick snett, därav kunde vi inte visa denna sida.
                </p>
                <p> Ifall du vill anmäla en matförgiftning </p>
                <a class="btn btn-primary btn-lg" href="/rapportera" role="button">Anmäl här</a>
            </div>
        </div>
    </div>

@endsection
