@extends('layouts.app')

@section('content')

    <div class="container">



        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Något gick väldigt tokigt!</h1>

                <p>
                    Vi vet inte vad som hände. Något har gått fel, tekniskt. Du får gärna skicka ett meddelande till oss på matförgiftad.se(snabela)gmail.com och berätta vad du gjorde, detta hjälper oss enormt i arbetat av att felsöka. Tack på förhand!
                </p>
            </div>
        </div>
    </div>

@endsection
