<div class="container-fluid photo-container">
    <div class="row">
        <div class="col-lg-12 photo-wrapper">
            <div class="photo">
                <img src='/images/header-small.jpg'
                     srcset='/images/header-Xsmall.jpg 500w,
                            /images/header-small.jpg 1000w,
                            /images/header-medium.jpg 1500w',
                            /images/header-big.jpg 2000w',
                            /images/original.jpg 4608w',
                />
            </div>
        </div>
    </div>
</div>