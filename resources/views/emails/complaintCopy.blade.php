@component('mail::message')
# Matförgiftning

Kopia av anmälan om matförgiftning

__Restaurang:__ {{$user_info->restaurant}}

__Adress:__ {{$user_info->streetaddress}}

__Maten förtärdes:__ {{$user_info->date}}

__klockan (ca):__ {{$user_info->time}}

__Antal personer som insjuknat:__ {{$user_info->sickCount}}

__Följande mat förtärdes:__

{{$user_info->sickFood}}

__Personerna som insjuknade visar följande symptom:__

@foreach($user_info->symptoms as $symptom)
- {{$symptom}}
@endforeach

__Antal timmar innan insjukning (påvisande av symptom):__ {{$user_info->timeUntilSymptoms}}

__Behövde person(erna) uppsöka vård?:__ @if($user_info->care) Ja @else Nej @endif
<br>

__Känd allergi:__ @if($user_info->allergy) Ja @else Nej @endif

<br>

__Övrigt:__

{{$user_info->otherData}}

@if(! $user_info->anonymous)
##Kontaktuppgifter
__Namn:__ {{$user_info->name or ''}}

__Epost:__ {{$user_info->email or ''}}

__Telefonnummer:__ {{$user_info->phone or ''}}
@else
Personen som anmälde valde tyvärr att vara anonym.
@endif
@endcomponent