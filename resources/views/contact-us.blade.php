@extends('layouts.app')

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1>Kontakt</h1>

                <p>Om du vill anmäla en misstänkt matförgiftning så använd <a href="/rapportera">detta formuläret.</a></p>

                <p>Kontakta oss gärna om du undrar något eller med kommentarer och idéer för hur vi kan göra matforgiftad.se till en bättre tjänst!</p>

                <p>Du kan nå oss på matforgiftad.se(snabel-a)gmail.com</p>

            </div>
        </div>
    </div>

@endsection
