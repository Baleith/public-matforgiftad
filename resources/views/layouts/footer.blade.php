<footer>
    <div class="container">
        <div class="row">
            <div class="col-12">
                <ul>
                    <li class="title"><a href="">Matförgiftad.se</a></li>
                    <li><a href="/">Hem</a></li>
                    <li><a href="/rapportera">Anmäl</a></li>
                    <li><a href="/matforgiftning">Fakta</a></li>
                    <li><a href="/om-matforgiftad">Om matförgiftad</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>