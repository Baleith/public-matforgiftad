<!doctype html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{$meta["title"] ?? "Matförgiftad.se | Anmäl misstänkt matförgiftning"}}</title>
    <meta name="description" content="{{$meta["description"] ?? "Anmäl matförgiftning. Det är många som drabbas årligen. Anmäl det till ansvarig kommun."}}">

    <link rel="canonical" href="{{ URL::current() }}"/>
    <!-- Icon -->{{--
    <link rel="icon" href ="/favicon.ico" type="image/x-icon" /> <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
--}}
<!-- Icon -->
    <link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
    <link rel="manifest" href="/site.webmanifest">
    <meta name="msapplication-TileColor" content="#da532c">
    <meta name="theme-color" content="#ffffff">
    <link href="{{ mix('/css/app.css') }}" rel="stylesheet" type="text/css">

    {{--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAOuhTKgO9En_VvYklDLfNfa1PKMsHj4tY&libraries=places"></script>--}}
    <script src="https://www.google.com/recaptcha/api.js?onload=vueRecaptchaApiLoaded&render=explicit" async defer>
    </script>
        
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-58459850-12"></script>
    <script>
      window.dataLayer = window.dataLayer || [];
      function gtag(){dataLayer.push(arguments);}
      gtag('js', new Date());
      gtag('config', 'UA-58459850-8');
      gtag('config', 'UA-58459850-12');
    </script>
    
</head>
<body>

    @include('layouts.nav')

    <main id="main">
        @yield('content')
    </main>

    @include('layouts.footer')

    <script src="{{ mix('/js/app.js') }}"></script>
</body>
</html>