<div class="container container-header">
    <nav class="navbar navbar-expand-lg navbar-light">
        <a class="navbar-brand" href="/">
            Matförgiftad
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="/">Hem <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/rapportera">Anmäla</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/matforgiftning">Fakta</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/om-matforgiftad">Om matförgiftad</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/kontakt">Kontakta oss</a>
                </li>
            </ul>
        </div>
    </nav>
</div>

@include('../ads/header')

<div class="container page-header">
    {{--<div class="row">
        <div class="col-8">--}}
            <div class="row justify-content-center">
                <div class="col-12 page-text-container">
                    <span>{{$title ?? ""}}</span> / <span>{{$desc ?? ""}}</span>
                </div>
            </div>
       {{-- </div>
    </div>--}}
</div>