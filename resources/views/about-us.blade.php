@extends('layouts.app')

@section('content')

    <div class="container">
        <h1>Om matförgiftad.se</h1>

        <p>
        Matförgiftad.se är en tjänst där du enkelt kan anmäla om du misstänker att du blivit matförgiftad på restaurang, café eller annan servering.
        </p>

        <p>
        Anmälan skickas till ansvarig kommun och du kan själv välja om du vill vara anonym eller inte.
        </p>

        <p>
        Tjänsten är helt kostnadsfri, både för dig som anmäler matförgiftning och för alla Sveriges kommuner som är med.
        Målet med matförgiftad.se är att öka andelen matförgiftningar som anmäls till kommunerna. I dagsläget anmäls endast 2-3000 av de uppskattade 500 000 matförgiftningar som sker varje år.
        </p>
    </div>

@endsection
