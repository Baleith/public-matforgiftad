<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('/rapportera', 'HomeController@report');
Route::get('/matforgiftning', 'HomeController@getFacts');
Route::get('/om-matforgiftad', 'HomeController@aboutUs');
Route::get('/kontakt', 'HomeController@contactUs');
Route::post('/complaint', 'ReportController@store');
Route::get('/rapportera/skickad', 'HomeController@sent')->name('skickad');


Route::get('/get/cities','ApiController@getCities');
